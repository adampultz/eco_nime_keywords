NIME 2020 paper: eco.nime
=========================

This repo contains assets and tools for a meta review of NIME proceedings to see if and where environmental issues have been addressed previously in NIME research. 

Methods: 

- automated keyword search of downloaded proceedings, using approach by Jensenius (NIME 2014) to investigate 'gesture', and adapted by Sullivan & Wanderley (SMC 2018) to explore DMI stability. 
- collocation and concordance analyses to understand context?

It might be good to run the keyword search first, then examine the papers/abstracts manually after. 

See [KEYWORDS.md](keywords.md) for list of keywords to search for.

The keywords search script is run in the [NIME_proceedings_pdf](NIME_proceedings_pdf) directory, which contains all papers by year. It creates a data.csv file with results by year (number of occurrances for each term, and list of papers). Note that it gets overwritten each time it is run. 
